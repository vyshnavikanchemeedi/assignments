var fs = require('fs')

fs.readFile('math.js','utf8', (err, data) => { 
    if(err){
        console.log(err);
    }
    console.log(data);
});

fs.writeFile("newfile.html",'<h1>hello</h1>',(err) => {
    if(err){
        console.log(err);
    }
    console.log("file created successfully");
});

fs.appendFile("newfile.html","<h2>welcome</h2>", (err) => {
    if(err){
        console.log(err);
    }
    console.log("data added to file");
});

fs.writeFile("demo.js",'console.log("hello");',(err) => {
    if(err){
        console.log(err);
    }
    console.log("file created successfully");
});

fs.rename("demo.js","index.js", (err) => {
    if(err){
        console.log(err);
    }
    console.log("file name modified");
});

fs.writeFile("demo.txt",'welcome to hyderabad',(err) => {
    if(err){
        console.log(err);
    }
    console.log("file created successfully");
});

fs.unlink("demo.txt",(err) => {
    if(err){
        console.log(err);
    }
    console.log("file deleted");
});








